# Chatbot

![chatbot](https://cdn.pixabay.com/photo/2019/03/21/15/51/chatbot-4071274_960_720.jpg)

## Recherche d'informations

- Qu'est-ce qu'une intention (intent) pour un chatbot ?

- Qu'est-ce qu'une entité (entity) pour un chatbot ?

- Qu'est-ce qu'une phrase de déclenchement (Trigger phrase) pour un chatbot ? 

- Définir les étapes de création d'un chatbot

- Qu'est-ce qu'un bon chatbot ?

- A quoi sert Q&A maker ?

- A quoi sert LUIS ?

- A quoi servent les bibliothèques Python aiohttp et cookiecutter ?

## Activité 1 : écho bot

- Créer un environnement virtuel et installer les bibliothèques Python botbuilder-core, aiohttp et cookiecutter==1.7.0

- Initialiser le bot à l'aide de la commande `cookiecutter https://github.com/microsoft/botbuilder-python/releases/download/Templates/echo.zip`

- Donner un nom et une description à votre bot

- Aller dans le dossier du bot et lancer le bot à l'aide de la commande `python app.py`

- Installer le logiciel Bot Framework Emulator (https://github.com/microsoft/BotFramework-Emulator/blob/master/README.md)

- Lancer le logiciel Bot Framework Emulator

- Cliquer sur "Open Bot" et entrer l'url `http://localhost:3978/api/messages`

- Tester le bon fonctionnement du bot

- Changer le code Python pour que le bot réponde en français (phrase d'accueil et écho des réponses)

## Activité 2 : météo bot

- Créer un compte gratuit sur l'API de météo OpenWeather : https://openweathermap.org/

- Créer une fonction Python pour récupérer la température à Grenoble

- Modifier le fichier `bot.py` pour que le bot renvoie la température à Grenoble si la demande de l'utilisateur du bot contient les mots "température" et "Grenoble"

- Quelles sont les limites de cette manière de programmer le bot ?

## Activité 3 : météo bot en mieux

Par rapport à la partie précédente, on va ajouter un modèle de machine learning pour comprendre les demandes des utilisateurs du bot.

- ouvrir l'interface de LUIS : https://www.luis.ai/applications

- créer une nouvelle application avec les réglages en français

- créer une intention (intent) "connaitre la température"

- créer des exemples de phrases (utterances) pour cette intention. Les phrases doivent pouvoir prendre en compte les entités (entity) de lieux

- créer une intention (intent) "connaitre coucher soleil'"

- créer des exemples de phrases (utterances) pour cette intention. Les phrases doivent pouvoir prendre en compte les entités (entity) de lieux

- créer une intention (intent) "trouver un apprenant pour présenter son travail"

- créer des exemples de phrases (utterances) pour cette intention. Les phrases doivent pouvoir prendre en compte une entité (entity) d'apprenant de la formation non concerné par le tirage au sort

```python
list_apprenants = [
    "Thienvu",
    "Marouan",
    "Cinthya",
    "Aissa",
    "Nolan",
    "William",
    "Armand",
    "Merouane",
    "Gabriel",
    "Cyrille",
    "Thomas",
    "Rayane",
    "Kamel",
    "David",
]
```

- entrainer le modèle

- tester le modèles pour les 3 intentions

- publier le modèle

- tester le endpoint d'API avec du code Python (requests)

- créer 3 fonctions Python pour les 3 intentions : récupérer la température dans un lieu donné, récupérer l'heure de coucher de soleil dans un lieu donné et récupérer le prénom d'un apprenant aléatoirement

- connecter votre bot à l'API de votre modèle LUIS et à vos 3 fonctions Python (aucune clé d'API ne doit apparaitre dans le code)

- vérifier le bon fonctionnement du bot

## Activité 4 : bot discord

- Créer un channel discord pour le bot

- Ouvrir le portail des développeurs de Discord : https://discord.com/developers/applications

- Créer une nouvelle application et ajouter un bot

- Aller dans l'onglet "OAuth2" et cocher "bot" dans l'onglet "scopes" et "Administrator" dans "Bot permissions"

- Copier l'url et connecter le bot au channel discord dont vous avez les droits administrateur

- Installer la bibliothèque "discord" dans un environnement virtuel Python

- Récupérer la clé d'API du bot et l'ajouter dans une variable d'environnement

- Copier et adapter le code suivant (en particulier avec dotenv):

```python
import discord

client = discord.Client()

@client.event
async def on_ready():
    print("Le bot est prêt !")

@client.event
async def on_message(message):
    if "hello" in message.content:
        await message.channel.send("Salut je suis le bot discord")

client.run(TOKEN)
```
- Vérifier que le bot fonctionne correctement

